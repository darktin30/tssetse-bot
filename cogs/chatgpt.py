from discord.ext import commands
from dotenv import load_dotenv
import openai
import discord
import os


import asyncio
import typing # For typehinting
import functools

async def setup(bot: commands.Bot):
        await bot.add_cog(ChatGPT(bot))

class ChatGPT(commands.Cog):
    def __init__(self, bot):
        load_dotenv()
        
        self.bot = bot

        self.previous_answers = []
        self.last_request_successful = True

        self.model_engine = "text-davinci-003"
        self.model_id = "text-davinci-003"
        openai.organization = os.getenv("OPENAI_ORGANIZATION_ID")
        openai.api_key = os.getenv("OPENAI_API_KEY")

    def to_thread(func: typing.Callable) -> typing.Coroutine:
        @functools.wraps(func)
        async def wrapper(*args, **kwargs):
            return await asyncio.to_thread(func, *args, **kwargs)

        return wrapper

    def append_answer_context(self, question, answer):
        self.previous_answers.append(f"Q: {question}\nA: {answer}\n\n")

    def reset_context(self):
        self.previous_answers = []

    def build_question_prompt(self, question, all_previous_answers: bool = True):
        header = """Beantworte die Frage so ehrlich wie möglich, basierend auf dem zur Verfügung stehenden Kontext."""

        if len(self.previous_answers) == 0:
            prompt = header + f"\n\n Q: {question} \n A:"
            return prompt

        if all_previous_answers:
            prompt = header + "\n".join(self.previous_answers) + f"\n\n Q: {question} \n A:"
        else:
            prompt = header + "\n" + self.previous_answers[-1] + f"\n\n Q: {question} \n A:"
            return prompt

        return prompt

    @to_thread
    def chatgpt_completion(self, question):
        prompt = self.build_question_prompt(question, all_previous_answers=True)

        completions = openai.Completion.create(
            engine=self.model_engine,
            prompt=prompt,
            max_tokens=3400,
            n=1,
            stop=None,
            temperature=0.5,
        )

        try:
            answer = completions.choices[0].text
            print(answer)
            self.append_answer_context(question, answer)
        except openai.error.InvalidRequestError as e:
            if not self.last_request_successful:
                return f"Multiple requests raised errors:\n{e}"
            self.last_request_successful = False
            self.reset_context()
            return None

        self.last_request_successful = True
        return answer

    @to_thread
    def chatgpt_answer(self, prompt):
        answer = openai.Answer.create(
            search_model="ada",
            model="curie",
            question=prompt,
            max_rerank=10,
            max_tokens=5,
            stop=["\n", "<|endoftext|>"]
        )

        return answer["answers"][0]

    @commands.command()
    async def gpt(self, ctx, *, prompt: str):
        async with ctx.typing():
            if prompt == "reset":
                self.reset_context()
                message = "Reset memory!"
            else:
                message = await self.chatgpt_completion(prompt)

                if message is None:
                    message = await self.chatgpt_completion(prompt)

            if len(message) > 4096:
                embed = discord.Embed(title="ChatGPT", description=f"{message[:4090]} ...")
                embed.add_field(name="Error", value="Answer was longer then 4096, truncating ...", inline=False)
            else:
                embed = discord.Embed(
                    title="ChatGPT",
                    description=message
                )
            if message is None:
                await ctx.send("Response was none! ):")
            else:
                await ctx.send(embed=embed)
